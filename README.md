# LabelWindow

LabelWindow is an easy to use editor only class that allows to use Unity default label window for either selecting elements from a list, or for selecting labels. Even though it uses reflection to make the default Unity label window work it has been successfully tested with versions:
- 5.6.5
- 2017.1
- 2017.4
- 2018.1
- 2018.2
- 2018.3
- 2019

# Features

  - No need of an Object type to use the window.
  - Labels can be used anywhere and for anything.
  - Can auto populate full labels list. Including custom labels created on the inspector.
  - Custom labels created with this window are not saved into the labels list.

# Usage

The usage is very simple, all you need is a reference to the `LabelWindow` class assign your function to the `OnSelect` delegate and call the `OpenLabelWindow` function passing the draw position and the array of labels (which can be null).

```sh
LabelWindow selection = new LabelWindow();
selection.OnSelect = (string selectedLabel, bool added) =>
{
    ...
    Your logic of what to do with the selected element
    ...
};
selection.OpenLabelWindow(position, customLabelArray);
```

# Additional parameters
The `OpenLabelWindow` function has 7 more parameters (with default value) that can be used to customize the usage of the window. They are explained on detail below.

| Parameter name | Default value | Description |
|---|---|---|
| labels | null | Array used for populating the list, if null is passed it won't be used  |
| selectedLabels | null | Array of the selected labels, if null is passed it won't be used  |
| populateDefaultLabels | true | Should the default label list be populated  |
| allowMultipleSelection | true |  Should the system allow more than 1 label selected |
| closeOnSelection | false |  Should the window close on selection |
| allowCustom | true |  Allow the user to use custom labels |
| maxCount | 15 |  Max label count that can be selected |
| sortAlphabetically | true |  Should the label list be sorted |
| enableAutoCompletition | true |  Should the window show auto completetion text |

# Importing
Copy `LabelWindow.cs` to an Editor folder (this is important otherwise you will not be able to compile your game) or use Unity Package Manager git setup to import it. Add the following to your project packages.json `"rotaryheart.lib.labelwindow": "https://gitlab.com/RotaryHeart-UnityShare/LabelWindow.git"`